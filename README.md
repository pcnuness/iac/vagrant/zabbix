# Vagrant / Ansible / Zabbix / Grafana

## Índice

1. [Overview - What is Vagrant?](#overview)
2. [Version - Supported versions](#version)
3. [Setup Requirements - Prerequisites](#setup-requirements)
4. [Usage](#usage)
    - [Role Variables](#role-variables)
    - [Deploy](#deploy)
    - [Login](#login)

## Overview
---
* **Vagrant** is a tool designed to create virtual machines quickly and flexibly. It allows you to set up environments with the necessary configurations (OS, libraries, applications, etc.).

![Workflow](./evidencias/workflow.png)

## Version
The project has been tested with the following versions:

## Setup Requirements 
---

### Provider
- The software responsible for providing a virtualized environment. The project uses VirtualBox, an open-source solution.

```yaml
VirtualBox >= 5.2.8
```

### Provisioner
- Automation software that will prepare your machine, install packages and perform tasks. The option used in the project was Ansible.

```yaml
Ansible >= 2.5.0
```

For local development environment:

```yaml
VirtualBox >= 5.2.8
vagrant >= 2.0.4
Zabbix Version == 3.2.x or 3.4.x
Grafana >= 5.0.4

Instalar plugins do vagrant
- vagrant plugin install vagrant-hosts
- vagrant plugin install vagrant-hostsupdater
```

- [Vagrant downloads page](https://www.vagrantup.com/downloads.html)
- [VirtualBox downloads page](https://www.virtualbox.org/wiki/Downloads)

## Usage

### Role Variables
- All variables can be altered in the **vars** directory

```yaml
zabbix_server_Version: '<version>'
```

### Deploy
- Local **Vagrantfile**

```yaml
vagrant up zabbix
```

### Login

- http://127.0.0.1:8088
- (Admin / zabbix)

- http://127.0.0.1:8030
- (admin / admin)
